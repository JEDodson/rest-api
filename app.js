const express = require("express");
const Joi = require("joi");
const { nanoid } = require("nanoid");

// app is convention
const app = express();
// express.json() will allow content to be properly parsed to json (line 18)
app.use(express.json());

// pulls customer data from json file
let customers = require("./customers.json");

// get request when url/api/customers is entered into browser
app.get("/api/customers", (req, res) => {
  res.type("json");
  console.log("Success, sending customers...");
  res.send(JSON.stringify(customers));
});

app.get(`/api/customers/:customerId`, (req, res) => {
  const customer = customers.find(
    (c) => c.customerId === req.params.customerId
  );

  if (!customer) return res.status(404).send("Invalid customer.");
  res.send(customer);
});
// post request when url/api/customers is entered into browser, updates with req.body
app.post("/api/customers", (req, res) => {
  const { error } = validater(req.body);

  // if error key exists within validation object, return 400 bad request
  if (error) return res.status(400).send(`${error.message}`);
  // otherwise, add customer to array of customers

  const customer = {
    customerId: nanoid(),
    name: req.body.name,
  };

  customers.push(customer);
  res.send(`Successfully updated customer list, added ${req.body.name}`);
});

app.put(`/api/customers/:customerId`, (req, res) => {
  const customer = customers.find(
    (c) => c.customerId === req.params.customerId
  );
  const { error } = validater(req.body);

  if (error) return res.status(400).send(`${error}`);

  const oldName = customer.name;
  customer.name = req.body.name;
  res.send(
    `Successfully updated customer: ${oldName} has been updated to ${req.body.name}`
  );
});

app.delete(`/api/customers/:customerId`, (req, res) => {
  // Find customer by id
  const customer = customers.find(
    (c) => c.customerId === req.params.customerId
  );

  if (!customer) return res.status(404).send("Invalid customer.");
  // If the customer doesn't exist, throw a 404
  // Delete user
  customers = customers.filter((c) => c.customerId !== req.params.customerId);
  // Return deleted customer
  res.send(`Customer ID ${req.params.customerId} successfully deleted.`);
});

// validation to avoid reuse of code
const validater = (customer) => {
  const schema = Joi.object({
    name: Joi.string().min(5).required(),
  });

  return schema.validate(customer);
};
// defines port which selects the port on the environment variable, otherwise 3000 if not present
const port = process.env.PORT || 3000;
// listens for port, creates server
app.listen(port, () => {
  console.log(`listening on ${port}`);
});
